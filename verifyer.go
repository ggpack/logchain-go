package logchain

import (
	"bufio"
	"io"
)

type Verifyer struct {
	secret    []byte
	sigLength int

	Formatter
}

func NewVerifyer(opts *Options) *Verifyer {

	return &Verifyer{
		secret:    opts.Secret,
		sigLength: opts.SigLength,
		Formatter: opts.Formatter,
	}
}

func (this *Verifyer) Verify(iLogChain io.Reader) (bool, []byte, []byte) {

	var line, prevLine []byte
	firstLine := true

	scanner := bufio.NewScanner(iLogChain)
	for scanner.Scan() {
		prevLine = line
		line = scanner.Bytes()

		if firstLine {
			firstLine = false
			continue
		}

		aStoredSig, validSig := this.ExtractSignature(line)

		if validSig {
			aComputedSig := sign(prevLine, this.secret, this.sigLength)
			validSig = verify(aStoredSig, []byte(aComputedSig))
		}

		if !validSig {
			return false, line, prevLine
		}
	}

	return true, nil, nil
}
