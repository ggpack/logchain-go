package logchaintest

import (
	"log/slog"
)

// Keeping the log calls out of the test files
// so that the lines do not change that often.
func doLog1(logger *slog.Logger) {
	logger.Debug("Hello 🗣️") // TODO, set default logger in the test
	logger.Debug("le debug 🕵️")
	logger.Info("le info 🕊️")
	logger.Warn("le warning 😲")
	logger.Error("the error 🥊")
}
