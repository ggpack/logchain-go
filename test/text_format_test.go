package logchaintest

import (
	"bytes"
	"log/slog"
	"testing"

	"gitlab.com/ggpack/logchain-go/v2"
)

func toLines(buff bytes.Buffer) [][]byte {
	return bytes.Split(bytes.TrimSpace(buff.Bytes()), []byte("\n"))
}

func TestBasicConstruction(t *testing.T) {

	var logStream bytes.Buffer
	params := logchain.Params{
		"level":  slog.LevelDebug,
		"stream": &logStream,
	}
	options := logchain.Options{Secret: []byte("Et2FwQefvb7HfCb7tATguSicVj_7TVlM"), SigLength: 12, Formatter: logchain.TextFormatter{}}
	logger := logchain.NewLogger(params, &options)

	// Default debug logging
	firstMsg := "Hello 🗣️"
	logger.Info(firstMsg)

	lines := toLines(logStream)

	if len(lines) != 1 {
		t.Fatal("Incorrect number of log lines:", len(lines))
	}

	if !bytes.Contains(lines[0], []byte(firstMsg)) {
		t.Error("Wrong log writen", lines[0])
	}
}

func TestBasicLogLevels(t *testing.T) {

	var logStream bytes.Buffer
	params := logchain.Params{
		"stream":       &logStream,
		"seed":         []byte("cHJlbmV6IGVuIGRlIGxhIGdyYWluZSAhIOKAi/CfjYbigIvwn5Km4oCL8J+RhOKAiwo="),
		"level":        slog.LevelDebug,
		"timestampFmt": "mocked",
	}
	options := logchain.Options{Secret: []byte("4oCLR2cgbGUgYmcg8J"), SigLength: 12, Formatter: logchain.TextFormatter{}}
	logger := logchain.NewLogger(params, &options)

	doLog1(logger)

	lines := logStream.String()

	aExpectedOutput := `mocked DEBUG client1.go:10 Hello 🗣️ +gV/NNjTlGXV
mocked DEBUG client1.go:11 le debug 🕵️ qB15BSSQRUFR
mocked INFO client1.go:12 le info 🕊️ eLe7yEdcntOS
mocked WARN client1.go:13 le warning 😲 reDxc+ZbbEMI
mocked ERROR client1.go:14 the error 🥊 maGUsZVHmYyp
`

	if logStream.String() != aExpectedOutput {
		t.Errorf("\nExpected lines:\n%s\n\nGot:\n%s", aExpectedOutput, lines)
	}
}

/*func TestBasicInvalidFormat(t *testing.T) {
	params := logchain.Params{"template": "{{...}}"}

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	logchain.NewLogChainer(params)
}*/

func TestBasicCustomFields(t *testing.T) {

	var logStream bytes.Buffer
	params := logchain.Params{
		"stream":       &logStream,
		"seed":         []byte("cHJlbmV6IGVuIGRlIGxhIGdyYWluZSAhIOKAi/CfjYbigIvwn5Km4oCL8J+RhOKAiwo="),
		"level":        slog.LevelDebug,
		"timestampFmt": "mocked",
		"timestampUtc": true,
	}

	options := logchain.Options{Secret: []byte("4oCLR2cgbGUgYmcg8J+NkeKAi/CfjLbigIsK"), SigLength: 8, Formatter: logchain.TextFormatter{}}

	logger := logchain.NewLogger(params, &options)

	doLog3(logger)

	lines := logStream.String()

	aExpectedOutput := `mocked INFO client3.go:10 Before custom fields lmSE/fXi
mocked INFO client3.go:12 With custom fields ctx=1234 level=<nil> userId=Gg 🐺 and=more Wrru4pVb
mocked INFO client3.go:13 After custom fields HX9p/Am3
`

	if lines != aExpectedOutput {
		t.Error("Expected lines:\n", aExpectedOutput, "\nGot:\n", lines)
	}
}

func TestBasicVerifyHappyCase(t *testing.T) {

	chainReader := bytes.NewBufferString(`2024-10-04 22:53:50.867 INFO main.go:22 Hello the Logchain 🏁 /7H1
2024-10-04 22:53:50.867 INFO main.go:23 New entry product=ham H5r5
2024-10-04 22:53:50.867 WARN main.go:24 Request for listing type=4 bANU
2024-10-04 22:53:50.867 ERROR main.go:25 testing the fields aaa=bbb ccc=4 RvkY
2024-10-04 22:53:50.867 INFO main.go:27 A user action userId=Carlo coucou=4 gg=bg KS01`)

	options := logchain.Options{Secret: []byte("secu"), SigLength: 4, Formatter: logchain.TextFormatter{}}

	verifyer := logchain.NewVerifyer(&options)

	isValid, _, _ := verifyer.Verify(chainReader)
	if !isValid {
		t.Error("Error verifying the chain")
	}
}

func TestBasicVerifyMissingLine(t *testing.T) {

	chainReader := bytes.NewBufferString(`2024-10-04 22:53:50.867 INFO main.go:22 Hello the Logchain 🏁 /7H1
2024-10-04 22:53:50.867 INFO main.go:23 New entry product=ham H5r5
2024-10-04 22:53:50.867 WARN main.go:24 Request for listing type=4 bANU
2024-10-04 22:53:50.867 ERROR main.go:25 testing the fields aaa=bbb ccc=4 RvkY
2024-10-04 22:53:50.867 INFO main.go:27 A user action userId=Carlo coucou=4 gg=bg KS01`)

	options := logchain.Options{Secret: []byte("the_secret"), SigLength: 16, Formatter: logchain.TextFormatter{}}

	verifyer := logchain.NewVerifyer(&options)

	aLogChain := toLines(*chainReader)

	isValid, line, prevLine := verifyer.Verify(chainReader)
	if isValid {
		t.Error("Error, the chain verification should have failed")
	}
	if line == nil {
		t.Error("Error, the bad line should have been outputted")
	}

	if len(aLogChain) < 4 {
		t.Error("Error, bad logchain split, its length:", len(aLogChain))
	}

	if !bytes.Equal(line, aLogChain[1]) {
		t.Error("Error, the bad line is not well set:", string(line))
	}
	if prevLine == nil {
		t.Error("Error, the last good line should have been outputted")
	}
	if !bytes.Equal(prevLine, aLogChain[0]) {
		t.Error("Error, the last good line is not well set:", string(prevLine))
	}
}
