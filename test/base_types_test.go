package logchaintest

import (
	"testing"

	"gitlab.com/ggpack/logchain-go/v2"
)

func TestRegularParamsAccess(t *testing.T) {
	p := logchain.Params{"key1": 1, "key2": false}

	// Existing key
	if p.Get("key1", 0).(int) != 1 {
		t.Error("Wrong value for key1")
	}
	if p.Get("key2", true).(bool) {
		t.Error("Wrong value for key2")
	}
	if p.Get("key3", false).(bool) {
		t.Error("key3 should be missing")
	}
}

func TestNilParamsAccess(t *testing.T) {

	// Representative way of using params, through a function call
	func(p logchain.Params) {
		if p.Get("key1", 0).(int) != 0 {
			t.Error("key1 should be missing")
		}
		if p.Get("key2", false).(bool) {
			t.Error("key2 should be missing")
		}
	}(nil)
}
