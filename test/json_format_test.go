package logchaintest

import (
	"bytes"
	"encoding/json"
	"log"
	"log/slog"
	"testing"

	"gitlab.com/ggpack/logchain-go/v2"
)

func TestJsonContent(t *testing.T) {
	// Grab the logging output
	var logStream bytes.Buffer
	params := logchain.Params{
		"stream": &logStream,
	}
	options := logchain.Options{Secret: []byte("Et2FwQefvb7HfCb7tATguSicVj_7TVlM"), SigLength: 12, Formatter: logchain.JsonFormatter{}}
	logger := logchain.NewLogger(params, &options)
	slog.SetDefault(logger)

	firstMsg := "Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ] 🍌"

	// Put varied levels
	log.Print(firstMsg)
	slog.Info("this is useful info")
	slog.Warn("warning!")
	slog.Error("error message")

	lines := toLines(logStream)
	if len(lines) != 4 {
		t.Fatal("Incorrect number of log lines:", len(lines))
	}

	var firstRec map[string]string
	json.Unmarshal(lines[0], &firstRec)

	keys := []string{"signature", "timestamp", "level", "msg"}

	for _, key := range keys {
		_, found := firstRec[key]
		if !found {
			t.Errorf("Key '%s' not found in record", key)
		}
	}
	firstMsgRetrieved := firstRec["msg"]
	if firstMsgRetrieved != firstMsg {
		t.Errorf("Message not matching the expectation.\nCurrent : '%s'\nExpected: '%s'", firstMsgRetrieved, firstMsg)
	}
}

func TestJsonVerify(t *testing.T) {

	options := logchain.Options{Secret: []byte("Et2FwQefvb7HfCb7tATguSicVj_7TVlM"), SigLength: 12, Formatter: logchain.JsonFormatter{}}
	verifyer := logchain.NewVerifyer(&options)

	aLogChain := bytes.NewBufferString(`{"time":"2024-09-06T23:54:03.290568803+02:00","level":"INFO","msg":"Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ] 🍌","signature":"BXKSUUaS5iN+"}
{"time":"2024-09-06T23:54:03.290662454+02:00","level":"INFO","msg":"this is useful info","signature":"B8uP0zL5XQ9q"}
{"time":"2024-09-06T23:54:03.290669567+02:00","level":"WARN","msg":"warning!","signature":"Tdl6dlOJT1TY"}
{"time":"2024-09-06T23:54:03.29067478+02:00","level":"ERROR","msg":"error message","signature":"GUiqzj8WKJ7j"}
{"time":"2024-09-06T23:54:03.290679842+02:00","level":"ERROR","msg":"Good bye.","signature":"DKNyMpV49hsm"}`)

	result, line, prevLine := verifyer.Verify(aLogChain)
	if !result {
		t.Error("The chain should have been seen as valid")
	}
	if prevLine != nil {
		t.Error("Expected nil previous line, got", string(prevLine))
	}
	if line != nil {
		t.Error("Expected nil error line, got", string(line))
	}

	// Deletion
	aLogChain = bytes.NewBufferString(`{"time":"2024-09-06T23:54:03.290662454+02:00","level":"INFO","msg":"this is useful info","signature":"B8uP0zL5XQ9q"}
{"time":"2024-09-06T23:54:03.290679842+02:00","level":"ERROR","msg":"Good bye.","signature":"DKNyMpV49hsm"}`)
	result, _, _ = verifyer.Verify(aLogChain)
	if result {
		t.Error("The chain should have been seen as invalid")
	}

	// Tampering
	aLogChain = bytes.NewBufferString(`{"time":"2024-09-06T23:54:03.290669567+02:00","level":"WARN","msg":"No problemo!","signature":"Tdl6dlOJT1TY"}
{"time":"2024-09-06T23:54:03.29067478+02:00","level":"ERROR","msg":"error message","signature":"GUiqzj8WKJ7j"}
{"time":"2024-09-06T23:54:03.290679842+02:00","level":"ERROR","msg":"Good bye.","signature":"DKNyMpV49hsm"}`)
	result, _, _ = verifyer.Verify(aLogChain)
	if result {
		t.Error("The chain should have been seen as invalid")
	}
}

func TestJsonContextualFields(t *testing.T) {

	var logStream bytes.Buffer

	params := logchain.Params{
		"seed":         []byte("cHJlbmV6IGVuIGRlIGxhIGdyYWluZSAhIOKAi/CfjYbigIvwn5Km4oCL8J+RhOKAiwo="),
		"stream":       &logStream,
		"timestampFmt": "mocked",
	}
	options := logchain.Options{Secret: []byte("the_secret"), SigLength: 16, Formatter: logchain.JsonFormatter{}}
	logger := logchain.NewLogger(params, &options)

	doLog3(logger)

	aExpectedOutput := `{"timestamp":"mocked","level":"INFO","src":"client3.go:10","msg":"Before custom fields","signature":"1+llrXL7ObO8aR0o"}
{"timestamp":"mocked","level":"INFO","src":"client3.go:12","msg":"With custom fields","ctx":1234,"level":null,"userId":"Gg 🐺","and":"more","signature":"Wn2Qq+JhP6Nnv73o"}
{"timestamp":"mocked","level":"INFO","src":"client3.go:13","msg":"After custom fields","signature":"bgY+7eykc7A1WFP0"}
`

	lines := logStream.String()

	if lines != aExpectedOutput {
		t.Errorf("\nExpected lines:\n%s\n\nGot:\n%s", aExpectedOutput, lines)
	}
}
