package logchaintest

import (
	"log/slog"
)

// Keeping the log calls out of the test files
// so that the lines do not change that often.
func doLog3(logger *slog.Logger) {
	logger.Info("Before custom fields")
	contextualizedLogger := logger.With("ctx", 1234, "level", nil, "userId", "Gg 🐺")
	contextualizedLogger.Info("With custom fields", "and", "more")
	logger.Info("After custom fields")
}
