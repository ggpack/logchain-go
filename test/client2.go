package logchaintest

import (
	"log"
	"log/slog"
)

// Keeping the log calls out of the test files
// so that the lines do not change that often.
func doLog2(logger *slog.Logger) {
	log.Print("This debug should be skipped")
	logger.Debug("Never logged with default level")
	logger.Info("Never logged with default level")
	logger.Warn("Never logged with default level")
	logger.Error("🦈 bad news")
}
