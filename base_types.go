package logchain

import (
	"io"
	"log/slog"
)

type Params map[string]any

func (this Params) Get(key string, defaultVal any) any {
	if value, exists := this[key]; exists {
		return value
	} else {
		return defaultVal
	}
}

// Needed for type agnostic Handler returned
type HandlerCtor = func(w io.Writer, opts *slog.HandlerOptions) slog.Handler

type Formatter interface {
	HandlerCtor() HandlerCtor
	ExtractSignature([]byte) ([]byte, bool)
}

type Options struct {
	Secret    []byte
	SigLength int
	Formatter Formatter
}
