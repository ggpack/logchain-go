package logchain

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
)

// Generates a truncated signature of the input message.
// @param length: controls the size of the signature, set it to None for full length.
func sign(message, secret []byte, length int) string {
	mac := hmac.New(sha256.New, secret)
	mac.Write(message)
	return base64.StdEncoding.EncodeToString(mac.Sum(nil))[:length]
}

func verify(refMac, currMac []byte) bool {
	return hmac.Equal(refMac, currMac)
}

// Generates a cryptographically random string of the given length.
func GenerateRandomKey(length int) []byte {
	randStr := make([]byte, length)
	rand.Read(randStr)
	return randStr
}
