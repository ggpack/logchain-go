module example

go 1.22

require gitlab.com/ggpack/logchain-go/v2 v2.0.0

replace gitlab.com/ggpack/logchain-go/v2 v2.0.0 => ../
