package main

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/ggpack/logchain-go/v2"
)

func main() {
	log.Print("Logging via the standard package")

	params := logchain.Params{
		"seed": []byte("seed"),
	}

	options := logchain.Options{Secret: []byte("secu"), SigLength: 4, Formatter: logchain.TextFormatter{}}

	logger := logchain.NewLogger(params, &options)

	logger.Info("Hello the Logchain 🏁")
	logger.Info("New entry", "product", "ham")
	logger.Warn("Request for listing", "type", 4)
	logger.Error("testing the fields", "aaa", "bbb", "ccc", 4)
	subLogger := logger.With("userId", "Carlo")
	subLogger.Info("A user action", "coucou", 4, "gg", "bg")

	chainReader := strings.NewReader(`2024-10-04 22:46:37.320 INFO main.go:19 hello, world /7H1
2024-10-04 22:46:37.320 INFO main.go:20 New entry product=ham Suf0
2024-10-04 22:46:37.320 INFO main.go:21 Request for listing type=4 l2VW
2024-10-04 22:46:37.320 WARN main.go:22 test aaa=bbb ccc=4 T1M+
2024-10-04 22:46:37.320 INFO main.go:24 A user action userId=Carlo coucou=4 gg=bg 2/kN`)

	verifyer := logchain.NewVerifyer(&options)

	result, line, prevLine := verifyer.Verify(chainReader)
	fmt.Printf("Result: %t\n", result)

	if result {
		fmt.Println("All right")
	} else {

		fmt.Printf("Invalid chain:\n\tlast good: %s\n\tfirst bad: %s\n", prevLine, line)
	}
}
