package logchain

import (
	"bytes"
	"context"
	"io"
	"log/slog"
	"runtime"
	"slices"
	"strings"
)

// based on the default
type innerTextHandler struct {
	stream io.Writer
	opts   *slog.HandlerOptions

	customFields []slog.Attr
}

// 1988-07-14 17:57:40.005 D client1.go:10 Hello 🗣️ |966484fdf5e2005a

func (this *innerTextHandler) Enabled(_ context.Context, userLvl slog.Level) bool {

	return userLvl >= this.opts.Level.Level()
}

func getValue(rec slog.Record, key string) *slog.Value {

	var val *slog.Value

	rec.Attrs(func(attr slog.Attr) bool {
		result := attr.Key == key
		if result {
			val = &attr.Value
		}
		return !result
	})
	return val
}

func getAttrsExcept(rec slog.Record, excludedKeys ...string) []slog.Attr {

	var attrs []slog.Attr

	rec.Attrs(func(attr slog.Attr) bool {
		result := slices.Contains(excludedKeys, attr.Key)
		if !result {
			attrs = append(attrs, attr)
		}
		return true
	})

	return attrs
}

// Copied from std slog
func getSource(rec slog.Record) *slog.Source {
	fs := runtime.CallersFrames([]uintptr{rec.PC})
	f, _ := fs.Next()
	return &slog.Source{
		Function: f.Function,
		File:     f.File,
		Line:     f.Line,
	}
}

// Partial rewrite (improved) of the internal slog handler
func (this *innerTextHandler) Handle(ctx context.Context, rec slog.Record) error {

	var builder strings.Builder

	// Convert all record's members to attrs to then apply the common replacer
	var gs []string
	repTime := this.opts.ReplaceAttr(gs, slog.Time(slog.TimeKey, rec.Time.Round(0)))
	builder.WriteString(repTime.Value.String())
	builder.WriteString(" ")

	repLevel := this.opts.ReplaceAttr(gs, slog.String(slog.LevelKey, rec.Level.String()))
	builder.WriteString(repLevel.Value.String())
	builder.WriteString(" ")

	repSrc := this.opts.ReplaceAttr(gs, slog.Any(slog.SourceKey, getSource(rec)))
	builder.WriteString(repSrc.Value.String())
	builder.WriteString(" ")

	repMessage := this.opts.ReplaceAttr(gs, slog.String(slog.MessageKey, rec.Message))
	builder.WriteString(repMessage.Value.String())
	builder.WriteString(" ")

	for _, field := range this.customFields {
		builder.WriteString(field.String())
		builder.WriteString(" ")
	}

	// TODO remove duplicates
	lineAttrs := getAttrsExcept(rec, "signature")
	for _, attr := range lineAttrs {
		builder.WriteString(attr.String())
		builder.WriteString(" ")
	}

	signature := getValue(rec, "signature")
	if signature != nil {
		builder.WriteString(signature.String())
	}

	builder.WriteString("\n") // Same behaviour as the intenal slog handler

	// Single write, to save as previous line
	this.stream.Write([]byte(builder.String()))

	return nil
}

func (this *innerTextHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	copiedHandler := *this
	copiedHandler.customFields = append(copiedHandler.customFields, attrs...)

	return &copiedHandler
}

func (this *innerTextHandler) WithGroup(name string) slog.Handler {
	return this
}

type TextFormatter struct {
}

func (this TextFormatter) HandlerCtor() HandlerCtor {
	return func(stream io.Writer, opts *slog.HandlerOptions) slog.Handler {
		return &innerTextHandler{
			stream: stream,
			opts:   opts,
		}
	}
}

func (this TextFormatter) ExtractSignature(line []byte) ([]byte, bool) {

	sep := []byte(" ")
	sepIdx := bytes.LastIndex(line, sep)

	if sepIdx == -1 {
		return nil, false
	}

	line = line[sepIdx+len(sep):]
	endIdx := bytes.Index(line, []byte(" "))

	if endIdx == -1 {
		return line, true
	} else {
		return line[:endIdx], true
	}
}
