package logchain

import (
	"bytes"
	"context"
	"io"
	"log/slog"
	"os"
	"path/filepath"
	"strconv"
)

type Handler struct {
	secret    []byte
	sigLength int

	saver *lineSaver

	slog.Handler
}

func (this *Handler) Handle(ctx context.Context, rec slog.Record) error {

	rec.AddAttrs(slog.String(
		"signature",
		sign(this.saver.prevLine, this.secret, this.sigLength),
	))

	return this.Handler.Handle(ctx, rec)
}

func (this *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	copiedHandler := *this
	copiedHandler.Handler = copiedHandler.Handler.WithAttrs(attrs)

	return &copiedHandler
}

type lineSaver struct {
	io.Writer
	prevLine []byte
}

func NewLineSaver(params Params) *lineSaver {
	return &lineSaver{
		Writer:   params.Get("stream", os.Stdout).(io.Writer),
		prevLine: params.Get("seed", GenerateRandomKey(128)).([]byte),
	}
}

func (this *lineSaver) Write(line []byte) (n int, err error) {

	// Slog always adds newline right between Handle and Write and we do not want to have to sign the line with the newline
	this.prevLine = bytes.TrimSuffix(line, []byte("\n"))
	return this.Writer.Write(line)
}

// Construction of a log-chainer.
func NewLogger(params Params, opts *Options) *slog.Logger {
	saver := NewLineSaver(params)

	timestampFmt := params.Get("timestampFmt", "2006-01-02 15:04:05.000").(string)
	timestampUtc := params.Get("timestampUtc", false).(bool)

	handler := Handler{
		secret:    opts.Secret,    //.Get("secret", GenerateRandomKey(128)).([]byte),
		sigLength: opts.SigLength, // params.Get("sigLength", 16).(int),

		saver: saver,
		Handler: opts.Formatter.HandlerCtor()(saver, &slog.HandlerOptions{
			AddSource: true,
			Level:     params.Get("level", slog.LevelInfo).(slog.Level),
			ReplaceAttr: func(groups []string, attr slog.Attr) slog.Attr {

				switch attr.Key {
				case slog.TimeKey:
					now := attr.Value.Time()
					if timestampUtc {
						now = now.UTC()
					}
					return slog.Attr{Key: "timestamp", Value: slog.StringValue(now.Format(timestampFmt))}

				case slog.SourceKey:
					source := attr.Value.Any().(*slog.Source)
					attr.Value = slog.StringValue(filepath.Base(source.File) + ":" + strconv.Itoa(source.Line))
					attr.Key = "src"
				}
				return attr
			},
		}),
	}

	return slog.New(&handler)
}
