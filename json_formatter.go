package logchain

import (
	"encoding/json"
	"io"
	"log/slog"
)

type JsonFormatter struct {
}

func (this JsonFormatter) HandlerCtor() HandlerCtor {
	return func(w io.Writer, opts *slog.HandlerOptions) slog.Handler {
		return slog.NewJSONHandler(w, opts)
	}
}

type jsonLine struct {
	Signature string `json:"signature"`
}

func (this JsonFormatter) ExtractSignature(line []byte) ([]byte, bool) {
	var rec jsonLine
	json.Unmarshal(line, &rec)
	return []byte(rec.Signature), len(rec.Signature) != 0
}
